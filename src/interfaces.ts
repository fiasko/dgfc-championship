import { Team } from "./teams";

export interface Pilot {
  LastName: string;
  FirstName: string;
  FKPilot?: string;
  Handicap?: number
}

export interface Flight {
  IDFlight: string;
  Category: 'Gleitschirm';
  FlightStartTime: string;
  FlightEndTime: string;
  FlightDate: string;
  GliderClassification: string;
  BestTaskPoints: string;
  TakeoffLocation: string;
  LandingLocation: string;
  FirstName: string;
  LastName: string;
  FKPilot: string;
  LastLat: string;
  LastLng: string
}

export interface BestFlight {
  pilot: Pilot;
  start: string;
  end: string;
  bestFlight: number;
  classifiction: string;
  handicap: number;
  weightedPoints: number;
}

export interface OverkillFlight {
  pilotA: Pilot;
  pilotB: Pilot;
  factor: number;
}

export interface DayAnalysis {
  date: string;
  flights: (BestFlight | null)[];
  overkillFlight: OverkillFlight;
  commonFlightsFactor: number;
  commonLandingAndTakeoffFactor: number;
  totalPointsThisDay: number;
}

export interface TeamAnalysis {
  teamInfo: Team;
  days: DayAnalysis[];
  overview: {
    points: number[];
    bestDate: string;
    bestPoints: number;
  };
};

export interface Analysis {
  days: string[],
  teams: TeamAnalysis[]
}
