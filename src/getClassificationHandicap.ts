import { Pilot } from "./interfaces";


export function getClassificationHandicap(gliderClassification: string, pilot: Pilot) {

  if (pilot.Handicap != null) {
    return pilot.Handicap;
  }

  switch (gliderClassification) {
    case 'EN A':
      return 0.8;
    case 'EN B':
      return 0.9;
    case 'EN C':
      return 1.0;
    case 'EN D':
      return 1.1;
    default:
      return Infinity;
  }
}
