import { Flight } from "./interfaces";
import { Team } from "./teams";


export function getCommonFlightsFactor(flights: Flight[], team: Team) {

  const gliderType = team.type;
  const amount = flights.filter(f => f != null).length

  if (gliderType == 'Paraglider') {

    switch (amount) {
      case 0:
      case 1:
      case 2:
        return 1.0;
      case 3:
        return 1.1;
      case 4:
      case 5:
        return 1.2;
      default:
        return 1;
    }
  } else if (gliderType == "Hangglider") {

    switch (amount) {
      case 0:
      case 1:
      case 2:
        return 1.0;
      case 3:
        return 1.2;
      default:
        return 1;
    }
  }
}
