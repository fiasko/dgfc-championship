import { Pilot, Flight } from "./interfaces";


export function pilotToName(pilot: Pilot | null | Flight) {
  if (pilot == null) {
    return null;
  }
  return `${pilot.FirstName} ${pilot.LastName}`;
}