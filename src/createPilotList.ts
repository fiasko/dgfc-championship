import * as XLSX from 'xlsx';
import { getFlightsForDay } from './getFlightsForDay';
import { flightDates } from './flightDates';
import { Pilot } from './interfaces';
import { byLastnameThenFirstname } from './byLastnameThenFirstname';


(async function () {

    const pilots = flightDates.map(day => {

        const flights = getFlightsForDay(day)
        return flights.map(flight => ({
            FirstName: flight.FirstName,
            LastName: flight.LastName,
            FKPilot: flight.FKPilot,
        }))

    }).flat()


    // sort pilots by last name, then first name
    pilots.sort(byLastnameThenFirstname)

    const wb = XLSX.utils.book_new();

    const table = []

    table.push(['First name', 'Last name', 'FKPilot'])
    table.push(...pilots.map(pilot => [pilot.FirstName, pilot.LastName, pilot.FKPilot]))

    const ws = XLSX.utils.aoa_to_sheet(table);
    ws['!cols'] = [
        { wch: 20 },
        { wch: 20 },
        { wch: 20 },
    ];


    XLSX.utils.book_append_sheet(wb, ws, 'Pilots');

    XLSX.writeFile(
        wb,
        'pilots.xlsx',
    );
})()

