import { Flight, Pilot } from "./interfaces";

export function getBestFlight(pilot: Pilot, flights: Flight[]) {

  const sortedFlights = flights
  .filter(flight => flight.FirstName == pilot.FirstName && flight.LastName == pilot.LastName)

  // sort by task points in descending order
  .sort((a, b) => (+b.BestTaskPoints) - (+a.BestTaskPoints));
  
  return sortedFlights[0]

}
