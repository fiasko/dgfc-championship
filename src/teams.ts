import { Pilot } from './interfaces';

export type GliderType = "Paraglider" | "Hangglider";

export interface Team {
  name: string;
  type: GliderType
  members: Pilot[];
}

export const teams = [
  {
    name: 'GlamourGliders',
    type: 'Paraglider',
    members: [
      {
        FirstName: "Jörg Peter",
        LastName: "Pfisterer",
      }, {
        FirstName: "Jürgen",
        LastName: "Roth",
      }, {
        FirstName: "Hannes",
        LastName: "Meyer-Schönbohm",
      }, {
        FirstName: "Guillaume",
        LastName: "Martin",
      }, {
        FirstName: "Jan",
        LastName: "Pfeiffer",
      },
    ]
  },
  {
    name: 'Puddingbrumseln',
    type: 'Paraglider',
    members: [
      {
        FirstName: "Henning",
        LastName: "Liebeck",
      }, {
        FirstName: "Thomas",
        LastName: "Hartenbach",
      }, {
        FirstName: "Daniel",
        LastName: "Römer",
      }, {
        FirstName: "Iñaki",
        LastName: "Nunez Ruiz",
      }, {
        FirstName: "Johannes",
        LastName: "Karl",
      },
    ]
  },
  {
    name: 'Thermiktüten',
    type: 'Paraglider',
    members: [
      {
        FirstName: "Natalie",
        LastName: "Roloff",
      }, {
        FirstName: "Jürgen",
        LastName: "Böhm",
      }, {
        FirstName: "Rolf",
        LastName: "Strittmatter",
      }, {
        FirstName: "Andrea",
        LastName: "Veit",
      }, {
        FirstName: "Matthias",
        LastName: "Zähringer",
      },
    ]
  },
  {
    name: 'Tieffluggeschwader Kandel',
    type: 'Paraglider',
    members: [
      {
        FirstName: "Manuela",
        LastName: "Rieger",
      }, {
        FirstName: "Rainer",
        LastName: "Disch",
      }, {
        FirstName: "Sajid",
        LastName: "Iqbal",
      }, {
        FirstName: "Steffen",
        LastName: "Winter",
      }, {
        FirstName: "Alexandra",
        LastName: "Silvelli",
      },
    ]
  },
  {
    name: 'Flughörnchen',
    type: 'Paraglider',
    members: [
      {
        FirstName: "K. Martin",
        LastName: "Bloße",
      }, {
        FirstName: "Marius",
        LastName: "Mönch",
      }, {
        FirstName: "Philipp",
        LastName: "Müller",
      }, {
        FirstName: "Jette",
        LastName: "Gööck",
      }, {
        FirstName: "Salome",
        LastName: "Fels",
      },
    ]
  },
  {
    name: 'Los tres cerditos',
    type: 'Paraglider',
    members: [
      {
        FirstName: "Michael",
        LastName: "Mari",
      }, {
        FirstName: "Olaf",
        LastName: "Hertwig",
      }, {
        FirstName: "Georg",
        LastName: "Bronkalla",
      }
    ]
  }, 
  {
    name: 'Wundertüten',
    type: 'Paraglider',
    members: [
      {
        FirstName: "Melanie",
        LastName: "Wehrle",
      }, {
        FirstName: "Thomas",
        LastName: "Ruf",
      }, {
        FirstName: "Thomas",
        LastName: "von Renner",
      }, {
        FirstName: "Martin",
        LastName: "Treubert",
      }, {
        FirstName: "Jakob",
        LastName: "Mayer",
      },
    ]
  },
  {
    name: 'Die wo gwinne welle',
    type: 'Hangglider',
    members: [
      {
        FirstName: "Roland",
        LastName: "Wöhrle",
        Handicap: 1
      },
      {
        FirstName: "Klaus",
        LastName: "Kienzle",
        Handicap: 1
      }
    ]
  },
  {
    name: 'Team 1',
    type: 'Hangglider',
    members: [
      {
        FirstName: "Marcel",
        LastName: "Kimpel",
        Handicap: 1
      },
      {
        FirstName: "Sven",
        LastName: "Hüninger",
        Handicap: 1
      }
    ]
  },
  {
    name: 'Dinos',
    type: 'Hangglider',
    members: [
      {
        FirstName: "Johannes",
        LastName: "Frank",
        Handicap: 1.0
      },
      {
        FirstName: "Diethard",
        LastName: "Ullrich",
        Handicap: 0.9
      },
      {
        FirstName: "Thomas",
        LastName: "Kuhlmann",
        Handicap: 0.9
      }
    ]
  }
] as Team[];
