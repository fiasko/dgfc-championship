import { readFileSync, writeFileSync } from 'fs';
import pug from 'pug';

const resultContents = readFileSync('./results.json', 'utf-8' )
const result = JSON.parse(resultContents)

const html = pug.renderFile('src/views/analysis.pug', result)

writeFileSync('result.html', html)