import axios from 'axios'
import { flightDates } from './flightDates';
import fs from 'fs'
import { join } from 'path'

(async function() {

    for(const day of flightDates) {
        const data = await getFlightsData(day);
        
        // write data to file
        writeToJsonFile(day, data);
    }

})()


function writeToJsonFile(day: string, data: any) {
    const path = join(__dirname, '../data/days', `${day}.json`);
    fs.writeFileSync(path, JSON.stringify(data, null, 2));
}

function flightsUrlForDay(day: string) {
    return `https://de.dhv-xc.de/api/fli/flights?d=${day}&s=2024&l-s=2023%20%2F%202024&fkto%5B%5D=9692&l-fkto%5B%5D=Kandel%20(DE)&navpars=%7B%22start%22%3A0%2C%22limit%22%3A250%2C%22sort%22%3A%5B%7B%22field%22%3A%22FlightDate%22%2C%22dir%22%3A-1%7D%2C%7B%22field%22%3A%22BestTaskPoints%22%2C%22dir%22%3A-1%7D%5D%7D`
}

async function getFlightsData(day: string) {
    const flightsUrl = flightsUrlForDay(day);
    const response = await axios.get(flightsUrl);

    console.log(`Day ${day}: Downloaded ${response.data.data.length} flights.`)
    
    const data = response.data.data;
    return data;
}

