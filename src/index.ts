import { Team, teams } from './teams';
import {
  Analysis,
  BestFlight,
  DayAnalysis,
  Flight,
  OverkillFlight,
  Pilot,
  TeamAnalysis,
} from './interfaces';
import { getBestFlight } from './getBestFlight';
import { pilotToName } from './pilotToName';
import { getClassificationHandicap } from './getClassificationHandicap';
import { getCommonFlightsFactor } from './getCommonFlightsFactor';

import * as XLSX from 'xlsx';
import { flightDates } from './flightDates';
import { getFlightsForDay } from './getFlightsForDay';
import { getDistance } from 'geolib';
import { writeFileSync } from 'fs';

const wb = XLSX.utils.book_new();

async function check() {

  const overview = []

  overview.push(["", ... flightDates, "Best day points", "best day", "Type"])

  const teamAnalysis = teams.map((team) => {
    console.log('-------------- ' + team.name + ' -------------- ');

    const table = [] as any[]
    
    table.push([team.type])

    let pointsThisWeek = [] as number[];
    let pointsPerDay = [] as number[]
    const days = flightDates.map((date) => {

      table.push([date]);
      table.push([
        '',
        'Start',
        'Ende',
        'Bester Flug',
        'Klassifikation',
        'Handicap',
        'Gewichtete Punkte',
      ]);

      const allFlightsToday = getFlightsForDay(date);
      

      const flightsToday = [] as Flight[];

      let sumOfWeightedPoints = 0

      const bestFlights = team.members.map(pilot => {
        
        const bestFlight = getBestFlight(pilot, allFlightsToday);

        if (bestFlight == null) {
            table.push([pilotToName(pilot), 'No flights']);
            return null
        } else {

          const weightedPoints = +bestFlight.BestTaskPoints /
          getClassificationHandicap(bestFlight.GliderClassification, pilot);

          sumOfWeightedPoints += weightedPoints

          table.push([
            pilotToName(pilot),
            bestFlight.FlightStartTime,
            bestFlight.FlightEndTime,
            bestFlight.BestTaskPoints,
            bestFlight.GliderClassification,
            getClassificationHandicap(bestFlight.GliderClassification, pilot),
            weightedPoints,
          ]);
        }

        if (bestFlight != null) {
          flightsToday.push(bestFlight);
        }

        return {
          pilot: pilot,
          start: bestFlight?.FlightStartTime,
          end: bestFlight?.FlightEndTime,
          bestFlight: +bestFlight?.BestTaskPoints,
          classifiction: bestFlight?.GliderClassification,
          handicap: getClassificationHandicap(bestFlight?.GliderClassification, pilot),
          weightedPoints: +bestFlight?.BestTaskPoints /
            getClassificationHandicap(bestFlight?.GliderClassification, pilot),
        }
      }) as BestFlight[];

    
      // Check all member pairs if they landed and started within the same time (30 minutes)
      const overkillFlight = getOverkillFlight(team, flightsToday, date)

      const commonFlightsFactor = getCommonFlightsFactor(flightsToday, team);
      const commonLandingAndTakeoffFactor = overkillFlight.factor

      table.push(['XC Points', sumOfWeightedPoints]);
      table.push([]);
      table.push([
        'Gemeinsamer Flug',
        pilotToName(overkillFlight.pilotA),
        pilotToName(overkillFlight.pilotB),
        'Faktor',
        commonLandingAndTakeoffFactor,
      ]);
      table.push([
        'Flüge an diesem Tag',
        flightsToday.length,
        '',
        'Faktor',
        commonFlightsFactor,
      ]);

      const totalPointsThisDay = 
        sumOfWeightedPoints! *
        commonFlightsFactor! *
        commonLandingAndTakeoffFactor!

      pointsThisWeek.push(totalPointsThisDay)

      pointsPerDay.push(totalPointsThisDay)




      table.push([
          totalPointsThisDay,
          'Points (mit Bonus)',
        ]);
      table.push([]);
      table.push([]);

      return {
        commonFlightsFactor,
        commonLandingAndTakeoffFactor,
        date,
        flights: bestFlights,
        overkillFlight,
        totalPointsThisDay
      }

    }) as DayAnalysis[];

    // first index of highest value in totalPointsThisWeek
    const highestIndex = pointsThisWeek.indexOf(Math.max(...pointsThisWeek));
    const bestDate = flightDates[highestIndex];
    const bestPoints = pointsThisWeek[highestIndex];

    overview.push([ team.name, ... pointsPerDay, bestPoints, bestDate, team.type ])

    table.push(["Punktzahlen pro Tag"])
    table.push([...flightDates])
    table.push([...pointsPerDay])

    table.push(["Bester Tag", bestDate, "Punkte", bestPoints])


    addTeamTable(table, team, wb);

    return {
      days,
      teamInfo: team,
      overview: {
        points: pointsThisWeek,
        dayPoints: pointsPerDay,
        bestDate,
        bestPoints
      }
    }
    
  }) as TeamAnalysis[];

  var newTable: Analysis = {
    days: flightDates,
    teams: teamAnalysis
  }

  const overviewWs = XLSX.utils.aoa_to_sheet(overview);
  overviewWs['!cols'] = overview[0].map(d => ({ wch: 10 }));

  XLSX.utils.book_append_sheet(wb, overviewWs, 'Übersicht');

  XLSX.writeFile(
    wb,
    `results.xlsx`,
  );

  // write new table to file
  const json = JSON.stringify(newTable, null, 2);
  writeFileSync('./results.json', json);
}

function getOverkillFlight(team: Team, flightsToday: Flight[], date: string): OverkillFlight {

  let overkillTeamMemberA: Pilot | null = null;
  let overkillTeamMemberB: Pilot | null = null;
  let factor = 1;

  let takeoffAndLandingWithinTime = false;

  for (const memberA of team.members) {
    if (memberA == null) {
      continue;
    }

    if (takeoffAndLandingWithinTime) {
      break;
    }

    for (const memberB of team.members) {
      if (memberB == null) {
        continue;
      }

      if (takeoffAndLandingWithinTime) {
        break;
      }

      if (memberA == memberB) {
        break;
      }

      const bestFlightA = getBestFlight(memberA, flightsToday);
      const bestFlightB = getBestFlight(memberB, flightsToday);

      const startTimeA = new Date(bestFlightA?.FlightStartTime);
      const startTimeB = new Date(bestFlightB?.FlightStartTime);
      const endTimeA = new Date(bestFlightA?.FlightEndTime);
      const endTimeB = new Date(bestFlightB?.FlightEndTime);

      if (Math.abs(startTimeA.getTime() - startTimeB.getTime()) <= 30 * 60 * 1000) {
        if (Math.abs(endTimeA.getTime() - endTimeB.getTime()) <= 30 * 60 * 1000) {

          const distance = getFlightEndDistance(bestFlightA, bestFlightB);

          if (distance <= 500) {
            takeoffAndLandingWithinTime = true;
            console.log(date + ': start and end times within 30 minutes, distance: ' + distance.toFixed(0), [memberA, memberB].map(m => pilotToName(m)).join(' <-> '));
            overkillTeamMemberA = memberA;
            overkillTeamMemberB = memberB;
            factor = 2.5;
          }
        }
      }
    }
  }

  return {
    pilotA: overkillTeamMemberA!,
    pilotB: overkillTeamMemberB!,
    factor: factor,
  }
}

function getFlightEndDistance(bestFlightA: Flight, bestFlightB: Flight) {
  const coordinateA = {
    latitude: +bestFlightA.LastLat,
    longitude: +bestFlightA.LastLng
  };

  const coordinateB = {
    latitude: +bestFlightB.LastLat,
    longitude: +bestFlightB.LastLng
  };

  return getDistance(coordinateA, coordinateB);;
}

function addTeamTable(table: any[], team: Team, wb: XLSX.WorkBook) {
  const ws = XLSX.utils.aoa_to_sheet(table);
  ws['!cols'] = [
    { wch: 20 },
    { wch: 20 },
    { wch: 20 },
    { wch: 20 },
    { wch: 20 },
    { wch: 20 },
    { wch: 20 },
  ];
  XLSX.utils.book_append_sheet(wb, ws, team.name);
}

check();