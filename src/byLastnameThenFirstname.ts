import { Pilot } from "./interfaces";

export const byLastnameThenFirstname = (a: Pilot, b: Pilot): 0 | 1 | -1 => {
    if (a.LastName < b.LastName) {
        return -1;
    } else if (a.LastName > b.LastName) {
        return 1;
    } else {
        if (a.FirstName < b.FirstName) {
            return -1;
        } else if (a.FirstName > b.FirstName) {
            return 1;
        } else {
            return 0;
        }
    }
};
