import { readFileSync } from "fs";
import { join } from "path/posix";
import { Flight } from "./interfaces";

export function getFlightsForDay(date: string) {
  const fileContents = readFileSync(join(__dirname, `../data/days/${date}.json`));
  const flights = JSON.parse(fileContents.toString()) as Flight[];
  return flights;
}
