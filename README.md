# DGFC XC Teamwertung

Auswertetool für die DGFC Vereinsmeisterschaften 2022

## Vorher

Nodejs herunterladen und installieren (https://nodejs.org/). Möglicherweise muss der Rechner neu gestartet werde.

## Ausführen

Es gibt Shellscripts (Linux, macOS) bzw. Batchdateien im Hauptverzeichnis. Nach dem Download dieses Repositorys, muss zunächst `0_install_once` ausgeführt werden. Bei Windows reicht ein Doppelklick auf die Batchdatei, bei macOS muss mit Rechtsclick -> Öffnen gearbeitet werden, sonst verbietet macOS die Ausführung.

Dann kann `1_fetch_pilots` ausgeführt werden. Eine Pilotenliste `pilots.xlsx` wird erstellt.

`2_run_analysis`, das eigentliche Auswerteprogramm, natürlich auch. `results.xlsx` wird im Hauptverzeichnis erstellt.

## Teams editieren

im Ordner `src/` gibt es die Datei `teams.ts`. Die Struktur dieser Datei muss unbedingt beibehalten werden, es können aber natürlich Teams hinzugefügt bzw. verändert werden.

```typescript
export const teams = [
  {
    name: 'Die Wundertüten', // Team Name
    type: 'Paraglider', // Team-Typ ('Paraglider' und 'Hangglider' sind möglich. 
                        // Ändert den Multiplikator für die Anzahl der Flüge innerhalb eines Tages)
    members: [
      {
        FirstName: "Melanie", // Vorname, Nachname, FKPilot. Das Programm prüft, 
                              // ob die Schreibweise des Namens mit der auf DHV-XC übereinstimmt.
        LastName: "Wehrle",
        FKPilot: "13194" // Diese Nummer kann der Pilot selbst auf DHV-XC.de 
                         // unter "Mein Piloten- und Benutzerprofil" - Tab Benutzer "Pilot-ID" 
                         // nachsehen oder, falls der Pilot schon mit einem Flug auf der 
                         // Vereinsmeisterschaft gelistet ist, aus der pilots.xlsx ausgelesen werden.
      },
      // ... weitere Piloten
    ]
  },
  // ... weitere Teams
]
```

## Faktoren/Handicap editieren

in der `src/helpers.ts` gibt es zwei stellen. Einmal die `getCommonFlightsFactor(amount: number, gliderType: GliderType)` (L:111) und dann die `getClassificationHandicap(gliderClassification?: string)`.
